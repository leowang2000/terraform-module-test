terraform {
  backend "http" {}
}

module "gitlab_local_file" {
  source = "gitlab.com/gitlab-examples/gitlab-file/local"

  text = "Hello World"
  filename = "hello"
}

output "filesize_in_bytes" {
  value = module.gitlab_local_file.bytes
}
