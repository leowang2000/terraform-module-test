## GitLab Terraform Module example

This repository shows how to consume a module served by the Terraform module registry.

The module is built and published by another project: [https://gitlab.com/gitlab-examples/ops/tf-example-module](https://gitlab.com/gitlab-examples/ops/tf-example-module)

You can read more about [the module registry in our documentation](https://docs.gitlab.com/ee/user/packages/terraform_module_registry/index.html).
